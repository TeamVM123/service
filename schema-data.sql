CREATE DATABASE brokerapp;



CREATE TABLE brokerapp.strategylist(
id INT PRIMARY KEY,
strategyname VARCHAR(50) NOT NULL
);


CREATE TABLE brokerapp.strategyupdateinformation(
id INT AUTO_INCREMENT PRIMARY KEY,
strategyname VARCHAR(50) not NULL,
strategyid  INT not NULL,
newshortavg DOUBLE,
newlongavg DOUBLE,
newmovingavg DOUBLE,
oldshortavg DOUBLE,
oldlongavg DOUBLE,
oldmovingavg DOUBLE,
timeupdated time not NULL,
dateupdated date not NULL
);
CREATE TABLE brokerapp.strategyinformation(
strategyname VARCHAR(50) PRIMARY KEY,
strategyid  INT not NULL,
shortavgtime DOUBLE,
longavgtime DOUBLE,
stdtime DOUBLE,
numberoccur INT,
timecalculated time not NULL,
datecalculated date not NULL,
stockid VARCHAR(5) not NULL,
volume INT not NULL,
currentamount DOUBLE not NULL,
profit DOUBLE not NULL,
active BOOLEAN not NULL,
loss DOUBLE not NULL
);


CREATE TABLE brokerapp.transaction(
id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
strategyid  INT not NULL,
strategyname VARCHAR(50) NOT NULL,
buy BOOLEAN NOT NULL,
price DOUBLE NOT NULL,
quantity INT NOT NULL,
stockid VARCHAR(5) not NULL,
datetrade date,
timetrade time
);

INSERT INTO brokerapp.strategylist VALUES(0,"Bolinger Bond");
INSERT INTO brokerapp.strategylist VALUES(1,"Moving Averages");
INSERT INTO brokerapp.strategylist VALUES(2,"Open Close");
