FROM mysql:5.7.19
ADD ./schema-data.sql /docker-entrypoint-initdb.d
ENV MYSQL_ROOT_PASSWORD=c0nygre
EXPOSE 3306

