## Run and Setup

Service 
1. Run the command MVN Package to build the jar file.
2. Run docker-compose build
3. Run docker-compose up
4. The JVM should start within 30 seconds depending on your machines performance

---

## Troubleshooting Common Errors
 
1. SQL Mapping Error - There was a request made was not mapped meaning there was a request to the wrong address
2. Timeout - The servic containers cannot connect to the internet
3. Error Creating Entitiy Factory Manager - The link to the database is wrong, or the port mapping is incorret. (this error is the result of the database not being able to talk to the rest API)

