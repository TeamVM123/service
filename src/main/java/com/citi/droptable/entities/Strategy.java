package com.citi.droptable.entities;

import org.springframework.stereotype.Repository;

import javax.persistence.*;
import java.io.Serializable;


@Entity
@Table(name = "strategylist")
public class Strategy implements Serializable {

    @Id
    @Column(name="id")
    @GeneratedValue
    private int id;

    @Column(name="strategyname")
    private String strategyName;

    public Strategy(){}

    public Strategy(String strategyName) {
        this.strategyName = strategyName;
    }

    public int getStrategy() {
        return id;
    }

    public void setStrategy(int strategy) {
        this.id = strategy;
    }

    public String getStrategyName() {
        return strategyName;
    }

    public void setStrategyName(String strategyName) {
        this.strategyName = strategyName;
    }
}
