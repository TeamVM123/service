package com.citi.droptable.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Time;
import java.util.Date;

@Entity
@Table(name="strategyinformation")
public class StrategyInfo implements Serializable {


    @Id
    @Column(name="strategyname")
    private String name;
    @Column(name="strategyid")
    private int  strategyId;
    @Column(name="shortavgtime")
    private double shortAvgTime;
    @Column(name="longavgtime")
    private double longAvgTime;
    @Column(name="stdtime")
    private double stdDeviationTime;
    @Column(name="numberoccur")
    private int numberofOccur;
    @Column(name="timecalculated")
    private Time time;
    @Column(name="datecalculated")
    private Date date;
    @Column(name="stockid")
    private String stock;
    @Column(name="volume")
    private int volume;
    @Column(name="currentamount")
    private double amount;
    @Column(name="profit")
    private double profit;
    @Column(name="active")
    private boolean active;
    @Column(name="loss")
    private double loss;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getStrategyId() {
        return strategyId;
    }

    public void setStrategyId(int strategyId) {
        this.strategyId = strategyId;
    }

    public double getShortAvgTime() {
        return shortAvgTime;
    }

    public void setShortAvgTime(double shortAvgTime) {
        this.shortAvgTime = shortAvgTime;
    }

    public double getLongAvgTime() {
        return longAvgTime;
    }

    public void setLongAvgTime(double longAvgTime) {
        this.longAvgTime = longAvgTime;
    }

    public double getStdDeviationTime() {
        return stdDeviationTime;
    }

    public void setStdDeviationTime(double stdDeviationTime) {
        this.stdDeviationTime = stdDeviationTime;
    }

    public int getNumberofOccur() {
        return numberofOccur;
    }

    public void setNumberofOccur(int numberofOccur) {
        this.numberofOccur = numberofOccur;
    }

    public Time getTime() {
        return time;
    }

    public void setTime(Time time) {
        this.time = time;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getStock() {
        return stock;
    }

    public void setStock(String stock) {
        this.stock = stock;
    }

    public int getVolume() {
        return volume;
    }

    public void setVolume(int volume) {
        this.volume = volume;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public double getProfit() {
        return profit;
    }

    public void setProfit(double profit) {
        this.profit = profit;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public double getLoss() {
        return loss;
    }

    public void setLoss(double loss) {
        this.loss = loss;
    }

    public StrategyInfo(){}

    public StrategyInfo(String name, int strategyId, double shortAvgTime, double longAvgTime, double stdDeviationTime, int numberofOccur, Time time, Date date, String stock, int volume, double amount, double profit, boolean active, double loss) {
        this.name = name;
        this.strategyId = strategyId;
        this.shortAvgTime = shortAvgTime;
        this.longAvgTime = longAvgTime;
        this.stdDeviationTime = stdDeviationTime;
        this.numberofOccur = numberofOccur;
        this.time = time;
        this.date = date;
        this.stock = stock;
        this.volume = volume;
        this.amount = amount;
        this.profit = profit;
        this.active = active;
        this.loss = loss;
    }
}
