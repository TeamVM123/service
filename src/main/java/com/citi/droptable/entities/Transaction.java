package com.citi.droptable.entities;


import javax.persistence.*;
import java.io.Serializable;
import java.sql.Time;
import java.sql.Date;

@Entity
@Table(name="transaction")
public class Transaction implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private int id;

    @Column(name="strategyid")
    private int  strategy;

    @Column(name="strategyname")
    private String strategyName;

    @Column(name="buy")
    private boolean buy;

    @Column(name="price")
    private double price;

    @Column(name="quantity")
    private int quantity;

    @Column(name="stockid")
    private String stock;

    @Column(name="datetrade")
    private Date date;

    @Column(name="timetrade")
    private Time time;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getStrategy() {
        return strategy;
    }

    public void setStrategy(int strategy) {
        this.strategy = strategy;
    }

    public String getStrategyName() {
        return strategyName;
    }

    public void setStrategyName(String strategyName) {
        this.strategyName = strategyName;
    }

    public boolean isBuy() {
        return buy;
    }

    public void setBuy(boolean buy) {
        this.buy = buy;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getStock() {
        return stock;
    }

    public void setStock(String stock) {
        this.stock = stock;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Time getTime() {
        return time;
    }

    public void setTime(Time time) {
        this.time = time;
    }

    public Transaction(){}

    public Transaction(int strategy, String strategyName, boolean buy, double price, int quantity, String stock, Date date, Time time) {
        this.strategy = strategy;
        this.strategyName = strategyName;
        this.buy = buy;
        this.price = price;
        this.quantity = quantity;
        this.stock = stock;
        this.date = date;
        this.time = time;
    }
}
