package com.citi.droptable.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Time;
import java.util.Date;
@Entity
@Table(name="strategyupdateinformation")
public class StrategyUpdateInfo implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="id")
    private int primary_key;
    @Column(name="strategyid")
    private int  strategyid;
    @Column(name="strategyname")
    private String strategyName;
    @Column(name="newshortavg")
    private double newShortAvg;
    @Column(name="newlongavg")
    private double newLongAvg;
    @Column(name="newmovingavg")
    private double newMovingAvg;
    @Column(name="oldshortavg")
    private double oldShortAvg;
    @Column(name="oldlongavg")
    private double oldlLongAvg;
    @Column(name="oldmovingavg")
    private double oldmovingavg;
    @Column(name="timeupdated")
    private Time timeupdated;
    @Column(name="dateupdated")
    private Date dateupdated;

    public int getStrategyid() {
        return strategyid;
    }

    public void setStrategyid(int strategyid) {
        this.strategyid = strategyid;
    }

    public int getPrimary_key() {
        return primary_key;
    }

    public void setPrimary_key(int primary_key) {
        this.primary_key = primary_key;
    }

    public String getStrategyName() {
        return strategyName;
    }

    public void setStrategyName(String strategyName) {
        this.strategyName = strategyName;
    }

    public double getNewShortAvg() {
        return newShortAvg;
    }

    public void setNewShortAvg(double newShortAvg) {
        this.newShortAvg = newShortAvg;
    }

    public double getNewLongAvg() {
        return newLongAvg;
    }

    public void setNewLongAvg(double newLongAvg) {
        this.newLongAvg = newLongAvg;
    }

    public double getNewMovingAvg() {
        return newMovingAvg;
    }

    public void setNewMovingAvg(double newMovingAvg) {
        this.newMovingAvg = newMovingAvg;
    }

    public double getOldShortAvg() {
        return oldShortAvg;
    }

    public void setOldShortAvg(double oldShortAvg) {
        this.oldShortAvg = oldShortAvg;
    }

    public double getOldlLongAvg() {
        return oldlLongAvg;
    }

    public void setOldlLongAvg(double oldlLongAvg) {
        this.oldlLongAvg = oldlLongAvg;
    }

    public double getOldmovingavg() {
        return oldmovingavg;
    }

    public void setOldmovingavg(double oldmovingavg) {
        this.oldmovingavg = oldmovingavg;
    }

    public Time getTimeupdated() {
        return timeupdated;
    }

    public void setTimeupdated(Time timeupdated) {
        this.timeupdated = timeupdated;
    }

    public Date getDateupdated() {
        return dateupdated;
    }

    public void setDateupdated(Date dateupdated) {
        this.dateupdated = dateupdated;
    }

    public StrategyUpdateInfo(){}

    public StrategyUpdateInfo(int strategyid, String strategyName, double newShortAvg, double newLongAvg, double newMovingAvg, double oldShortAvg, double oldlLongAvg, double oldmovingavg, Time timeupdated, Date dateupdated) {
        this.strategyid = strategyid;
        this.strategyName = strategyName;
        this.newShortAvg = newShortAvg;
        this.newLongAvg = newLongAvg;
        this.newMovingAvg = newMovingAvg;
        this.oldShortAvg = oldShortAvg;
        this.oldlLongAvg = oldlLongAvg;
        this.oldmovingavg = oldmovingavg;
        this.timeupdated = timeupdated;
        this.dateupdated = dateupdated;
    }
}
