package com.citi.droptable.controllers;

import com.citi.droptable.entities.Transaction;
import com.citi.droptable.services.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.sql.Time;
import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping("/api/transaction")
@CrossOrigin // allows requests from all domains
public class TransactionsController {

    @Autowired
    private TransactionService service;

    @RequestMapping(method = RequestMethod.GET, value = "/{id}")
    public Transaction getTransactionById(@PathVariable("id") int id) {
        return service.getTransactionById(id);
    }


    @RequestMapping(method = RequestMethod.GET, value ="/after/{id}")
    public List<Transaction> getTransactionByAfter(@PathVariable("id") int id2){
        return service.getTransactionByAfter(id2);
    }

    @RequestMapping(method = RequestMethod.DELETE,value = "/{id}")
    public void deleteTransactionById (@PathVariable("id") int id)
    {
        service.deleteTransactionById(id);
        
    }

    @RequestMapping(method = RequestMethod.DELETE)
    public void deleteTransaction(@RequestBody Transaction tran){
        service.deleteTransaction(tran);
    }

    @RequestMapping(method = RequestMethod.POST)
    public Transaction addTransaction(@RequestBody Transaction transaction){
        return service.createTransaction(transaction);
    }
    @RequestMapping(method = RequestMethod.PUT)
    public void updateTransaction(@RequestBody Transaction transaction){
        service.updateTransaction(transaction);
    }

}
