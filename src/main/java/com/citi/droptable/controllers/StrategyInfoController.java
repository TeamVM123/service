package com.citi.droptable.controllers;

import com.citi.droptable.entities.StrategyInfo;
import com.citi.droptable.services.StrategyInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/StrategyInfo")
@CrossOrigin // allows requests from all domains
public class StrategyInfoController {
    @Autowired
    private StrategyInfoService service;

    @RequestMapping(method = RequestMethod.GET, value = "/{strategyname}")
    public StrategyInfo getStrategyInfoById(@PathVariable("strategyname") String strategyName) {
        return service.getStrategyInfoById(strategyName);
    }
    @RequestMapping(method = RequestMethod.GET, value = "/")
    public List<StrategyInfo> getAllStrategyInfo(){
        return service.getAllStrategyInfo();
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
    public void deleteStrategyInfo(@PathVariable("id") String id) {
        service.deleteStrategyInfoById(id);
    }


    @RequestMapping(method = RequestMethod.PUT)
    public void updateByStrategyInfo(@RequestBody StrategyInfo strat){
        service.updateStrategyInfo(strat);
    }


    @RequestMapping(method = RequestMethod.DELETE)
    public void deleteStrategyInfo(@RequestBody StrategyInfo strategy) {
        service.deleteStrategyInfo(strategy);
    }

    @RequestMapping(method = RequestMethod.POST)
    public void addStrategyInfo(@RequestBody StrategyInfo strat){
        service.createStrategyInfo(strat);
    };
    
}
