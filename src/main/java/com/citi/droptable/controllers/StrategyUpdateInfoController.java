package com.citi.droptable.controllers;

import com.citi.droptable.entities.Strategy;
import com.citi.droptable.entities.StrategyUpdateInfo;
import com.citi.droptable.services.StrategyUpdateInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/StrategyUpdateTable")
@CrossOrigin // allows requests from all domains
public class StrategyUpdateInfoController {
    @Autowired
    private StrategyUpdateInfoService service;

    @RequestMapping(method = RequestMethod.GET, value = "/{id}")
    public StrategyUpdateInfo getStrategyUpdateInfoById(@PathVariable("id") int id) {
        return service.getStrategyById(id);
    }
    @RequestMapping(method = RequestMethod.DELETE,value = "/{id}")
    public void deleteStrategyUpdateInfoById (@PathVariable("id") int id)
    {
        service.deleteStrategyUpdateInfoById(id);
    } @RequestMapping(method = RequestMethod.DELETE)

    public void deleteStrategyUpdateInfo(@RequestBody StrategyUpdateInfo strat){
        service.deleteStrategyUpdateInfo(strat);
    }

    @RequestMapping(method = RequestMethod.POST)
    public void addStrategyUpdateInfo(@RequestBody StrategyUpdateInfo strat){
        service.createStrategyUpdateInfo(strat);
    }
    @RequestMapping(method = RequestMethod.PUT)
    public void updateStrategyUpdateInfo(@RequestBody StrategyUpdateInfo strat){
        service.updateStrategyUpdateInfo(strat);
    }

}
