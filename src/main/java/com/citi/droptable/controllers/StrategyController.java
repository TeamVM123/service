package com.citi.droptable.controllers;

import com.citi.droptable.entities.Strategy;
import com.citi.droptable.services.StrategyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/Strategy")
@CrossOrigin // allows requests from all domains
public class StrategyController {
    @Autowired
    private StrategyService service;

    @RequestMapping(method = RequestMethod.GET, value = "/{id}")
    public Strategy getStrategyById(@PathVariable("id") int id) {
        return service.getStrategyById(id);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
    public void deleteStrategy(@PathVariable("id") int id) {
        service.deleteStrategyById(id);
    }

    @RequestMapping(method = RequestMethod.DELETE)
    public void deleteStrategy(@RequestBody Strategy strategy) {
        service.deleteStrategy(strategy);
    }

    @RequestMapping(method = RequestMethod.POST)
    public void addStrategy(@RequestBody Strategy strat){
        service.createStrategyInfo(strat);
    }
    @RequestMapping(method = RequestMethod.PUT)
    public void updateStrategy(@RequestBody Strategy strat){
        service.updateStrategy(strat);
    }
}
