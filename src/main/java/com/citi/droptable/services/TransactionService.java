package com.citi.droptable.services;

import com.citi.droptable.entities.StrategyInfo;
import com.citi.droptable.entities.Transaction;

import java.util.Collection;
import java.util.List;

public interface TransactionService {
    Transaction getTransactionById(int id);
    void deleteTransactionById (int id);
    void deleteTransaction (Transaction transaction);

    void updateTransaction (Transaction transaction);

    Transaction createTransaction (Transaction transaction);
    List<Transaction> getTransactionByAfter(int id);

}
