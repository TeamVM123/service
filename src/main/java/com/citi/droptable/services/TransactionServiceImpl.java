package com.citi.droptable.services;

import com.citi.droptable.entities.Transaction;
import com.citi.droptable.repos.TransactionsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;

@Service
public class TransactionServiceImpl implements TransactionService {

    @Autowired
    private TransactionsRepository transactionsRepository;

    @Override
    public Transaction getTransactionById(int id) {
        return transactionsRepository.findById(id).get();
    }


    @Override
    public void deleteTransactionById(int id) {
        transactionsRepository.deleteById(id);
    }

    @Override
    public void deleteTransaction(Transaction transaction) {
        transactionsRepository.delete(transaction);
    }

    @Override
    public void updateTransaction(Transaction transaction) {
        transactionsRepository.save(transaction);
    }

    @Override
    public Transaction createTransaction(Transaction transaction) {
        return transactionsRepository.save(transaction);
    }

    @Override
    public List<Transaction> getTransactionByAfter(int id) {
        return transactionsRepository.getTransactionByAfter(id);
    }


}
