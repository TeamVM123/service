package com.citi.droptable.services;



import com.citi.droptable.entities.StrategyUpdateInfo;
import com.citi.droptable.repos.StrategyUpdateInfoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StrategyUpdateInfoServiceImpl implements StrategyUpdateInfoService {

    @Autowired
    private StrategyUpdateInfoRepository strategyUpdateInfoRepository;

    @Override
    public StrategyUpdateInfo getStrategyById(int id) {
        return strategyUpdateInfoRepository.findById(id).get();
    }

    @Override
    public void deleteStrategyUpdateInfoById(int id) {
        strategyUpdateInfoRepository.deleteById(id);
    }

    @Override
    public void deleteStrategyUpdateInfo(StrategyUpdateInfo strategyUpdateInfo) {
        strategyUpdateInfoRepository.delete(strategyUpdateInfo);
    }

    @Override
    public void updateStrategyUpdateInfo(StrategyUpdateInfo strategyUpdateInfo) {
        strategyUpdateInfoRepository.save(strategyUpdateInfo);

    }

    @Override
    public StrategyUpdateInfo createStrategyUpdateInfo(StrategyUpdateInfo strategyUpdateInfo) {
        return strategyUpdateInfoRepository.save(strategyUpdateInfo);
    }
}
