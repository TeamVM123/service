package com.citi.droptable.services;

import com.citi.droptable.entities.Strategy;
import com.citi.droptable.repos.StrategyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StrategyServiceImpl implements StrategyService {

    @Autowired
    private StrategyRepository strategyRepository;

    public Strategy getStrategyById(int id) {
        return strategyRepository.findById(id).get();
    }

    public void deleteStrategyById(int id) {
        strategyRepository.deleteById(id);

    }

    public void deleteStrategy(Strategy strategy) {
        strategyRepository.delete(strategy);

    }

    public void updateStrategy(Strategy strategyInfo) {
        strategyRepository.save(strategyInfo);

    }

    public Strategy createStrategyInfo(Strategy strategyInfo) {

        return strategyRepository.save(strategyInfo);
    }
}
