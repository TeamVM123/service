package com.citi.droptable.services;

import com.citi.droptable.entities.StrategyUpdateInfo;

public interface StrategyUpdateInfoService {

    StrategyUpdateInfo getStrategyById(int id);

    void deleteStrategyUpdateInfoById (int id);
    void deleteStrategyUpdateInfo (StrategyUpdateInfo strategyUpdateInfo);

    void updateStrategyUpdateInfo(StrategyUpdateInfo strategyUpdateInfo);

    StrategyUpdateInfo createStrategyUpdateInfo (StrategyUpdateInfo strategyInfo);
}
