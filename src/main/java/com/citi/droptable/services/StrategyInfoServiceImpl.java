package com.citi.droptable.services;

import com.citi.droptable.entities.StrategyInfo;
import com.citi.droptable.repos.StrategyInfoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StrategyInfoServiceImpl implements StrategyInfoService {

    @Autowired
    private StrategyInfoRepository strategyInfoRepository;

    public StrategyInfo getStrategyInfoById(String id) {
       return strategyInfoRepository.findById(id).get();
    }

    public List<StrategyInfo> getAllStrategyInfo(){return strategyInfoRepository.findAll();}

    public void deleteStrategyInfoById(String id) {
        StrategyInfo toBeDeleted = strategyInfoRepository.findById(id).get();
        deleteStrategyInfo(toBeDeleted);

    }

    public void deleteStrategyInfo(StrategyInfo strategyInfo) {
        strategyInfoRepository.delete(strategyInfo);

    }

    public void updateStrategyInfo(StrategyInfo strategyInfo)
    {
        StrategyInfo note = strategyInfoRepository.findById(strategyInfo.getName()).get();

        note.setActive(strategyInfo.isActive());

        strategyInfoRepository.save(note);

        }

    public StrategyInfo createStrategyInfo(StrategyInfo strategyInfo) {
       return strategyInfoRepository.save(strategyInfo);
    }
}
