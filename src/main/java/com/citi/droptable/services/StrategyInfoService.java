package com.citi.droptable.services;

import com.citi.droptable.entities.StrategyInfo;

import java.util.List;

public interface StrategyInfoService {

    StrategyInfo getStrategyInfoById(String id);

    void deleteStrategyInfoById (String id);
    void deleteStrategyInfo (StrategyInfo strategyInfo);
    List<StrategyInfo> getAllStrategyInfo();
    void updateStrategyInfo (StrategyInfo strategyInfo);

    StrategyInfo createStrategyInfo (StrategyInfo strategyInfo);


}
