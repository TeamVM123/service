package com.citi.droptable.services;

import com.citi.droptable.entities.Strategy;

public interface StrategyService {

    Strategy getStrategyById(int id);

    void deleteStrategyById (int id);
    void deleteStrategy (Strategy strategy);

    void updateStrategy (Strategy strategyInfo);

    Strategy createStrategyInfo (Strategy strategyInfo);
}
