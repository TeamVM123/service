package com.citi.droptable.repos;

import com.citi.droptable.entities.StrategyInfo;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;



@Repository
public interface StrategyInfoRepository extends JpaRepository<StrategyInfo, String> {

}
