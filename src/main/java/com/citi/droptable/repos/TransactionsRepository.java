package com.citi.droptable.repos;

import com.citi.droptable.entities.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.sql.Time;
import java.util.Collection;
import java.util.List;

@Repository
public interface TransactionsRepository extends JpaRepository<Transaction, Integer> {
    @Query(value = "SELECT * FROM transaction t WHERE t.id >= :id2",nativeQuery = true)
    List<Transaction> getTransactionByAfter(@Param("id2") int id2);
}
