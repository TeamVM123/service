package com.citi.droptable.repos;

import com.citi.droptable.entities.StrategyUpdateInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StrategyUpdateInfoRepository extends JpaRepository<StrategyUpdateInfo, Integer> {
}
